﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    
    public enum cardType {
        attack,
        armor,
        heal,
        fireball,
        total
    }
    
    public enum contestantType {
        player,
        enemy
    }
    
    public enum scenes {
        deckBuild,
        battleField
    }
    public Transform[] puzzleField = new Transform[2];
    public string currScene;
    
    public int cardAmt = 20;
    public int maxCombo = 5;
    public GameObject[] contestants = new GameObject[2];
    
    public int[] cardDupeInDeckLimit = new int[] {
        30,
        15,
        15,
        10
    };
    public int cardSizeLimit = 60; 
    
    private int cardAtkAmt = 2;
    private int cardArmAmt = 1;
    private int cardVitAmt = 1;
    private int cardBrnAmt = 3;
    
    public float mercyTime = 2.5f;
    public float mercyTime2nd = 1.5f;
    //public SpriteRenderer sprite;
    
    void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }

    public void FaceUpCardsCheck(GameObject contestant, List<GameObject> faceUpCardsObj) {
        ContestantStats contestantScr = contestant.GetComponent<ContestantStats>();
        // check if two cards are faced up
        if (faceUpCardsObj.Count >= 2) {
            CardData[] faceUpCardsScr = {
                faceUpCardsObj[0].GetComponent<CardData>(),
                faceUpCardsObj[1].GetComponent<CardData>()
            };
            int[] currCont = CheckPlayer(contestantScr.isPlayer);
            // check if match
            if (faceUpCardsScr[0].cardType == faceUpCardsScr[1].cardType) {
                if (contestantScr.combo < maxCombo) {
                    contestantScr.combo++; // increase combo meter of current contestant
                }
                int cardTypeVal = faceUpCardsScr[0].cardType;
                
                ContestantStats opponentData = contestants[currCont[1]].GetComponent<ContestantStats>();
                Debug.Log("hp: " + opponentData.HP);
                switch (cardTypeVal) {
                    case (int) cardType.attack:
                        opponentData.InflictDmg(cardAtkAmt * contestantScr.combo);
                    break;
                    
                    case (int) cardType.armor:
                        contestantScr.armor += cardArmAmt * contestantScr.combo;
                        contestantScr.HealthCheck();
                    break;
                    
                    case (int) cardType.heal:
                        contestantScr.HP += cardVitAmt * contestantScr.combo;
                        contestantScr.HealthCheck();
                    break;
                    
                    case (int) cardType.fireball:
                        opponentData.InflictBurn(cardBrnAmt * contestantScr.combo);
                    break;
                }
                
                faceUpCardsScr[0].locked = faceUpCardsScr[1].locked = true; // lock the card
                contestantScr.matchedCount++;     // increment matched cards count
                contestantScr.matchedCardsList.Add(faceUpCardsObj[0]);
                contestantScr.matchedCardsList.Add(faceUpCardsObj[1]);
                contestantScr.faceUpCardsObj.Clear(); // clear the list
                if (contestantScr.matchedCount >= cardAmt / 2) {
                    ResetField(contestantScr, currCont[0]);
                    contestantScr.matchedCount = 0;
                }
            } else {
                if (contestantScr.combo != 0) {
                    ResetField(contestantScr, currCont[0]);
                    contestantScr.combo = 0;  // reset combo
                }
                Debug.Log("combo is: " + contestantScr.combo);
            }
        }
    }
    
    private void MercyInit(float time, ContestantStats contestantScr, int currCont) {
        contestantScr.mercyInitialized = false;
        contestantScr.timerMercy = time;
        FlipAllCards(true, currCont);
    }
    
    // flip cards depending on parameter, used for Mercy timer and combo reset card replacement;
    public void FlipAllCards(bool flipUp, int currCont) {
        for (int i = 0; i < puzzleField[currCont].childCount; i++) {
            CardData card = puzzleField[currCont].GetChild(i).GetComponent<CardData>();
            card.locked = card.faceUp = flipUp;
            Transform grandChild = puzzleField[currCont].GetChild(i).GetChild(0);
            grandChild.gameObject.SetActive(flipUp);
        }
    }
    private void ResetField(ContestantStats contestantScr, int currCont) {
        CardAdd.AddNewCards(
            puzzleField[currCont], 
            contestants[currCont], 
            contestantScr.matchedCardsList.Count,
            currCont,
            contestantScr.matchedCardsList
        );
        contestantScr.matchedCardsList.Clear();
        MercyInit(mercyTime2nd, contestantScr, currCont);
        contestantScr.faceUpCardsObj.Clear(); // clear the list
    }
    
    public bool CardClicked(GameObject card, GameObject team, bool faceUp, bool locked, ContestantStats teamScr, int cardType) {
        if (currScene == "BattleField") {   // if in battlefield scene, cards acts as face up or down
            if (!faceUp && teamScr.faceUpCardsObj.Count < 2) {
                card.transform.GetChild(0).gameObject.SetActive(true);
                // add the current card from the list of face up cards
                teamScr.faceUpCardsObj.Add(card);
                Debug.Log("flipping up");
                FaceUpCardsCheck(team, teamScr.faceUpCardsObj);
                return true;
            } else if (faceUp && !locked) {
                card.transform.GetChild(0).gameObject.SetActive(false);
                // remove the current card from the list of face up cards
                teamScr.faceUpCardsObj.Remove(card);
                Debug.Log("flipping down");
                return false;
            } else {
                Debug.Log("it is locked");
            }
        } else { // else in deck builder and the cards act as just a button
            if (PlayerData.playerDeck.ContainsKey(cardType)) {
                if (PlayerData.playerDeck[cardType] < cardDupeInDeckLimit[cardType]) { 
                    PlayerData.cardSize++;
                    PlayerData.playerDeck[cardType]++;
                    DeckBuildManager.Instance.UpdateCardValues(cardType);
                }
            } else {
                PlayerData.cardSize++;
                PlayerData.playerDeck.Add(cardType, 1);
                DeckBuildManager.Instance.UpdateCardValues(cardType);
            }
            
            foreach (KeyValuePair<int, int> pair in PlayerData.playerDeck) {
                if (pair.Key == cardType) {
                    Debug.Log("Key: " + pair.Key + " | Value: " + pair.Value);
                }
            }
        }
        return true;
    }
    
    private int[] CheckPlayer(bool isPlayer) {
        if (isPlayer) {
            return new int[] {(int) contestantType.player, (int) contestantType.enemy};
        } else {
            return new int[] {(int) contestantType.enemy, (int) contestantType.player};
        }
    }
    
    public string GetSceneNameByIndex(int index) {
        string path = SceneUtility.GetScenePathByBuildIndex(index);
        string sceneName = path.Substring(0, path.Length - 6).Substring(path.LastIndexOf('/') + 1);
        return sceneName;
    }
    
    public void GameOver(bool isPlayer) {
        if (!isPlayer) {
            Debug.Log("Game Win");
        } else {
            Debug.Log("Game Lose");
        }
        currScene = GetSceneNameByIndex(0);
        Debug.Log("Current Scene: " + currScene);
        SceneManager.LoadScene(0);
    }
}
