﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardData : MonoBehaviour
{
    public int cardID;
    public int cardType;
    public GameObject team;
    public ContestantStats teamScr;
    public bool faceUp = false;
    public bool locked = false;
    private GameManager gameManagerScr;
    //void showGame
    void Start() {
        if (IsBattle()) {
            teamScr = team.GetComponent<ContestantStats>();
        }
        UpdateCardValue();
    }
    
    public void SendData() {
        faceUp = GameManager.Instance.CardClicked(gameObject, team, faceUp, locked, teamScr, cardType); 
    }
    
    public void UpdateCardValue() {
        transform.GetChild(0).gameObject.GetComponent<Text>().text = cardType.ToString();
    }
    
    private bool IsBattle() {
        if (GameManager.Instance.currScene == "BattleField")
            return true;
        else
            return false;
    }
}
