﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeckBuildManager : MonoBehaviour
{
    public static DeckBuildManager Instance { get; private set; }
    public Transform displayAvailCards;
    public Transform displayAddedCards;
    public GameObject btnPrefab;
    public GameObject btnAvailPrefab;
    public Text displaySize;
    public List<GameObject> displayDeck = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        GameObject button;
        CardData buttonData;
        // add available cards
        for (int i = 0; i < (int) GameManager.cardType.total; i++) {
            button = Instantiate(btnPrefab);
            button.name = "Card " + (i+1);
            button.transform.SetParent(displayAvailCards, false);
            button.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
            buttonData = button.GetComponent<CardData>();
            buttonData.cardType = i;
            buttonData.cardID = i+1;
        }
        
        foreach (KeyValuePair<int, int> pair in PlayerData.playerDeck) {
            GameObject newDisplayCard = Instantiate(btnAvailPrefab);
            newDisplayCard.transform.SetParent(displayAddedCards, false);
            displayDeck.Add(newDisplayCard);
            DisplayCard displayCardScr = newDisplayCard.GetComponent<DisplayCard>();
            displayCardScr.cardType = pair.Key;
            displayCardScr.cardAmt = pair.Value;
            displayCardScr.UpdateDisplay();
        }
        
        UpdateCardValues();
    }
    
    public void UpdateCardValues(int cardType = -4) {
        displaySize.text = PlayerData.cardSize.ToString() + " / " + GameManager.Instance.cardSizeLimit;
        bool contains = false;
        if (cardType != -4) {
            for (int i = 0; i < displayDeck.Count; i++) {
                DisplayCard displayCardScr = displayDeck[i].GetComponent<DisplayCard>();
                if (displayCardScr.cardType == cardType) {
                    displayCardScr.cardAmt++;
                    displayCardScr.UpdateDisplay();
                    contains = true;
                }
            }
            if (!contains) {
                GameObject newDisplayCard = Instantiate(btnAvailPrefab);
                newDisplayCard.transform.SetParent(displayAddedCards, false);
                displayDeck.Add(newDisplayCard);
                DisplayCard displayCardScr = newDisplayCard.GetComponent<DisplayCard>();
                displayCardScr.cardType = cardType;
                displayCardScr.cardAmt++;
                displayCardScr.UpdateDisplay();
            }
        }
    }
    public void DeductCard(GameObject card) {
        DisplayCard cardData = card.GetComponent<DisplayCard>();
        if (cardData.cardAmt > 1) {
            cardData.cardAmt--;
            PlayerData.playerDeck[cardData.cardType]--;
            cardData.UpdateDisplay();
        } else {
            displayDeck.Remove(card);
            PlayerData.playerDeck.Remove(cardData.cardType);
            Destroy(card);
        }
        PlayerData.cardSize--;
        Instance.UpdateCardValues();
    }
    public void NewScene() {
        if (PlayerData.cardSize == GameManager.Instance.cardSizeLimit) {
            GameManager.Instance.currScene = GameManager.Instance.GetSceneNameByIndex(1);
            Debug.Log("Current Scene: " + GameManager.Instance.currScene);
            SceneManager.LoadScene(1);
        } else if (PlayerData.cardSize > GameManager.Instance.cardSizeLimit) {
            Debug.Log("Too much cards, cannot start");
        } else {
            Debug.Log("Not enough cards, cannot start");
        }
    }
}
