﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCard : MonoBehaviour
{
    public int cardType;
    public int cardAmt = 0;
    
    public void SendData() {
        DeckBuildManager.Instance.DeductCard(gameObject);
    }
    
    public void UpdateDisplay() {
        transform.GetChild(0).gameObject.GetComponent<Text>().text = cardType.ToString() + " | " + cardAmt.ToString();
    }
}
