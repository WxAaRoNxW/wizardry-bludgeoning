﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField]
    private Transform Table;
    [SerializeField]
    private GameObject enemData;
    private ContestantStats enemDataScr;
    private float maxTime = 0.75f;
    private float timer;
    private int[] currCardSurrounding = new int[] {
        -6,-5,-4,
        -1,    1,
         4, 5, 6
    };
    private int[] randomWeightedRange = new int[] {
        0,1,2,3,4,
        5,6,7,8,9,
        10,11,12,13,14,
        15,16,17,18,19
    };
    private int randomCard;
    List<int[]> cardList = new List<int[]>();
    // Start is called before the first frame update
    void Start()
    {
        randomCard = Random.Range(0, randomWeightedRange.Length);
        timer = maxTime;
        enemDataScr = enemData.GetComponent<ContestantStats>();
    }
    private bool readInitialized = false;
    private int currCardClickedCount = 0;
    private int currCardIndex = 0;
    // Update is called once per frame
    void Update()
    {
        // let AI memorize a few cards
        if (enemDataScr.mercyInitialized == false) {
            if (!readInitialized) {
                // let AI focus on the highest amount of cards first
                // count the number of cards and duplicates in the field
                Dictionary<int, int> cardCount = new Dictionary<int, int>();
                for (int i = 0; i < Table.childCount; i++) {
                    CardData cardData = Table.GetChild(i).gameObject.GetComponent<CardData>();
                    if (cardCount.ContainsKey(cardData.cardType)) {
                        cardCount[cardData.cardType]++;
                    } else {
                        cardCount.Add(cardData.cardType, 1);
                    }
                }
                
                // add dictionary entries to a list to sort
                foreach (KeyValuePair<int, int> pair in cardCount) {
                    cardList.Add(new int[] {pair.Key, pair.Value});
                }
                
                // sort list
                for (int i = 0; i < cardList.Count-1; i++) {
                    for (int j = 0; j < cardList.Count-1; j++) {
                        int[] temp = cardList[j];
                        if (cardList[j][1] < cardList[j+1][1]) {
                            cardList[j] = cardList[j+1];
                            cardList[j+1] = temp;
                        }
                    }
                }
                
                for (int i = 0; i < cardList.Count; i++) {
                    Debug.Log("list: " + cardList[i][0] + " " + cardList[i][1]);
                }
                
                
                /*
                // focus on the highest cards with the highest amount of same type cards surrounding it
                for (int i = 0; i < Table.childCount; i++) {
                    cardData = Table.GetChild(i).gameObject.GetComponent<CardData>();
                    if (cardData.cardType == cardList[0][0]) {
                        
                    }
                }
                */
                
                // always remember the highest amount of cards somewhere around % of the time
                
                readInitialized = true;
            }
        } else {
            if (timer > 0) {
                timer -= 1 * Time.deltaTime;
            } else {
                for (int i = 0; i < Table.childCount; i++) {
                    CardData cardData = Table.GetChild(i).gameObject.GetComponent<CardData>();
                    if (cardData.cardType == cardList[currCardIndex][0] && !cardData.faceUp) {
                        cardData.SendData();
                        Debug.Log("chose card no: " + cardData.cardID);
                        currCardClickedCount++;
                        break;
                    }
                }
                if (currCardClickedCount >= cardList[currCardIndex][1]) {
                    if (currCardIndex < cardList.Count-1) {
                        currCardIndex++;
                        currCardClickedCount = 0;
                    } else { // Enemy has faced up all cards, reset everything
                        currCardIndex = 0;          // reset index position
                        currCardClickedCount = 0;   // reset clicked cards counter
                        readInitialized = false;    // reset memorization for when new cards are added
                        cardList.Clear();           // clear memorized cards to remember the next set of cards
                    }
                }
                timer = maxTime;
            }
        }
    }
}
