﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class LoadVariables : MonoBehaviour
{
    public Transform[] puzzleField = new Transform[2];
    public GameObject[] contestants = new GameObject[2];
    
    // load variables upon scene change
    void Awake()
    {
        GameManager instance = GameManager.Instance; 
        instance.puzzleField = puzzleField;
        instance.contestants = contestants;
        Destroy(gameObject);
    }
}
