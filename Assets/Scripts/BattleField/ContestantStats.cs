﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContestantStats : MonoBehaviour
{
    public bool isPlayer;
    public int contestantType;
    public int HP = 100;
    public int armor = 0;
    public int combo = 0;
    public int matchedCount = 0;
    public bool burn = false;
    private float burnTimePTick = 1f;
    private float burnCurrTime = 0;
    public Text displayHP;
    public Text displayArmor;
    public Text displayCombo;
    private int burnDmgMax = 0;
    private int burnCount = 0;
    public float timerMercy;
    public bool mercyInitialized = false;
    public List<GameObject> faceUpCardsObj = new List<GameObject>();
    public List<GameObject> matchedCardsList = new List<GameObject>();   // list for the list of cards that were matched, to be replaced with new cards
    public List<int> cardDeck = new List<int>();
    public List<int> graveYard = new List<int>();
    void Awake() {
        foreach (KeyValuePair<int, int> pair in PlayerData.playerDeck) {
            for (int j = 0; j < pair.Value; j++) {
                cardDeck.Add(pair.Key);
            }
        }
        for (int i = 0; i < PlayerData.playerDeck.Count; i++) {
            
        }
        RandomizeDeck();
    }
    void Start() {
        if (isPlayer) {
            contestantType = (int) GameManager.contestantType.player;
        } else {
            contestantType = (int) GameManager.contestantType.enemy;
        }
        timerMercy = GameManager.Instance.mercyTime;
    }
    // Update is called once per frame
    void Update()
    {
        // check if there's no status currently working
        if (burnDmgMax != 0) {
            // burn contestant
            if (burnCount < burnDmgMax) {
                if (burnCurrTime > 0) {
                    burnCurrTime -= burnTimePTick * Time.deltaTime;
                } else {
                    HP--;   // burn pierces armor
                    HealthCheck();
                    burnCurrTime = burnTimePTick;
                    burnCount++;
                }
            } else {
                burnCount = 0;
                burnDmgMax = 0;
            }
        }
        if (!mercyInitialized) {
            if (timerMercy > 0) {
                timerMercy -= 1 * Time.deltaTime;
            } else {
                GameManager.Instance.FlipAllCards(false, contestantType);
                mercyInitialized = true;
                Debug.Log("Flipping card");
            }
        }
        
        displayCombo.text = combo.ToString();
    }
    
    public void HealthCheck() {
        displayHP.text = HP.ToString();
        displayArmor.text = armor.ToString();
        if (HP <= 0) {
            GameManager.Instance.GameOver(isPlayer);
        }
    }
    public void InflictDmg(int dmg) {
        if (armor != 0) {
            if (dmg > armor) {
                dmg -= armor;
                armor = 0;
                HP -= dmg;
            } else {
                armor -= dmg;
            }
        } else {
            HP -= dmg;
        }
        HealthCheck();
    }
    public void InflictBurn(int dmg) {
        // check if character has not been afflicted with burn
        burnCurrTime = burnTimePTick;   // set burn time to the current tick rate
        burnDmgMax += dmg;            // queue the next damage to list
    }
    
    public void CheckDeck() {
        if (cardDeck.Count == 0) {
            cardDeck = new List<int>(graveYard);
            RandomizeDeck();
        }
        
    }
    
    private void RandomizeDeck() {
        // randomize deck of player
        for (int i = 0; i < cardDeck.Count; i++) {
            int temp = cardDeck[i];
            int randomIndex = Random.Range(i, cardDeck.Count);
            cardDeck[i] = cardDeck[randomIndex];
            cardDeck[randomIndex] = temp;
        }
    }
    
    public void cardClick() {
        
    }
}
