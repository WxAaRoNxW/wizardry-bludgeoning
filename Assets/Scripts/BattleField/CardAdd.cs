﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardAdd : MonoBehaviour
{
    
    private static GameObject[] btnPrefab;
    [SerializeField] 
    private GameObject[] _btnPrefab = new GameObject[2];
    
    void Start() {
        btnPrefab = _btnPrefab;
        // deal cards then randomize location of card in the field
        for (int i = 0; i < GameManager.Instance.puzzleField.Length; i++) {
            AddNewCards(GameManager.Instance.puzzleField[i], GameManager.Instance.contestants[i], GameManager.Instance.cardAmt, i);
        }
    }
    
    public static void AddNewCards(Transform currPuzzleField, GameObject contestant, int newCardAmt, int contestantType, List<GameObject> matchedCardsList = null) {
        for (int i = 0; i < newCardAmt; i++) {
            GameObject button;
            CardData buttonData;
            ContestantStats contestantData = contestant.GetComponent<ContestantStats>();
            // remove a card after every other increment since a there needs to be always a pair.
            if (i % 2 == 0 && i != 0) {
                contestantData.cardDeck.RemoveAt(0);
                contestantData.CheckDeck();
            }
            if (matchedCardsList != null) {
                button = matchedCardsList[i];
                buttonData = button.GetComponent<CardData>();
                contestantData.graveYard.Add(buttonData.cardType);
            } else {
                button = Instantiate(btnPrefab[contestantType]);
                button.name = "Card " + (i+1);
                button.transform.SetParent(currPuzzleField, false);
                buttonData = button.GetComponent<CardData>();
                buttonData.cardID = i+1;
                buttonData.team = contestant;
            }
            buttonData.cardType = contestantData.cardDeck[0];
            buttonData.UpdateCardValue();
            
        }
        
        RandomizeField(currPuzzleField);
    }
    
    static void RandomizeField(Transform currPuzzleField) {
        for (int i = 0; i < currPuzzleField.childCount; i++) {
            Transform child = currPuzzleField.GetChild(i);
            int randomIndex = Random.Range(i, currPuzzleField.childCount);
            child.SetSiblingIndex(randomIndex);
            /*
            ContestantStats.cardDeck[i] = ContestantStats.cardDeck[randomIndex];
            ContestantStats.cardDeck[randomIndex] = temp;
            */
        }
    }
}
